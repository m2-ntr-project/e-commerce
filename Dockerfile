# stage1 as builder
FROM node:lts-alpine as builder

WORKDIR /app

# Copy the package.json and install dependencies
COPY package*.json ./
RUN npm install

# Copy rest of the files
COPY . .

# Build the project
RUN npm run build


FROM tomcat:9.0
COPY --from=builder /app/dist /usr/local/tomcat/webapps/ecommerce
EXPOSE 8080

CMD ["catalina.sh", "run"]

export function addCart({ commit, getters }, payload) {
    if(payload.quantity >= 1) {
        let exist = false;
        let cart = getters.products
        cart.forEach(element => {
            if (element.product == payload.product) {
                exist = true;
                element.quantity = parseInt(element.quantity) + parseInt(payload.quantity);
            }
        })

        if (!exist) {
            cart.push(payload)
        }

        commit("setCart", cart)
    }
}

export function removeCart({ commit, getters }, id) {
    let cart = getters.products
    if (id) {
        for (let index = 0; index < getters.products.length; index++) {
            const element = getters.products[index];
            if (element.product.id === id) {
                cart.splice(index, 1);
            }
        }
    }
    commit("setCart", cart)
}

export function emptyCart({ commit }) {
    let cart = [];
    console.log("empty");
    commit("setCart", cart);
}